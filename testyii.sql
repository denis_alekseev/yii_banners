-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 25 2014 г., 10:46
-- Версия сервера: 5.5.34
-- Версия PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `testyii`
--
CREATE DATABASE IF NOT EXISTS `testyii` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `testyii`;

-- --------------------------------------------------------

--
-- Структура таблицы `ads_banners2`
--

DROP TABLE IF EXISTS `ads_banners2`;
CREATE TABLE IF NOT EXISTS `ads_banners2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `position` set('top','left','bottom','right') NOT NULL,
  `value` text,
  `file` text,
  `link` text,
  `counter` int(11) DEFAULT NULL,
  `propertyType` varchar(20) DEFAULT NULL,
  `dealKind` varchar(20) DEFAULT NULL,
  `dealDirection` varchar(20) DEFAULT NULL,
  `controller` varchar(20) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `ads_banners2`
--

INSERT INTO `ads_banners2` (`id`, `type`, `position`, `value`, `file`, `link`, `counter`, `propertyType`, `dealKind`, `dealDirection`, `controller`, `action`, `clicks`) VALUES
(1, 'html', 'left', 'sdfsdf', NULL, '', 50, '', '', '', '', '', 0),
(2, 'img', 'top', '', '', 'http://vk.com/alchemist_home', 2, '', '', '', '', '', 0),
(3, 'img', 'left', '', 'dodge-nitro-01.jpg', 'vk.com/widler', 0, 'residential', '', '', '', '', 0),
(4, 'img', 'bottom', '', 'dodge-nitro-01.jpg', 'vk.com/widler', 0, '', 'sale', '', '', '', 0),
(5, 'img', 'left', '', 'dodge-nitro-01.jpg', 'vk.com/widler', 0, '', 'sale', '', '', '', 0),
(6, 'img', 'left', '', 'dodje stratus.jpg', 'http://vk.com/widler', 50, '', '', '', '', '', 3),
(7, 'swf', 'top', '', 'test.swf', '', 17, '', '', '', '', '', 0),
(8, 'html', 'left', 'asdasdas <strong>test</strong>', NULL, '', 4, '', '', '', '', '', 0),
(9, 'html', 'left', 'asdasdas <strong>test</strong>', '', '', 7, '', '', '', '', '', 0),
(12, 'img', 'top', '', 'earth.jpg', '??????? ?????? asdasd asdasdas sadasd', 2, '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`) VALUES
(1, 'test1', 'pass1', 'test1@example.com'),
(2, 'test2', 'pass2', 'test2@example.com'),
(3, 'test3', 'pass3', 'test3@example.com'),
(4, 'test4', 'pass4', 'test4@example.com'),
(5, 'test5', 'pass5', 'test5@example.com'),
(6, 'test6', 'pass6', 'test6@example.com'),
(7, 'test7', 'pass7', 'test7@example.com'),
(8, 'test8', 'pass8', 'test8@example.com'),
(9, 'test9', 'pass9', 'test9@example.com'),
(10, 'test10', 'pass10', 'test10@example.com'),
(11, 'test11', 'pass11', 'test11@example.com'),
(12, 'test12', 'pass12', 'test12@example.com'),
(13, 'test13', 'pass13', 'test13@example.com'),
(14, 'test14', 'pass14', 'test14@example.com'),
(15, 'test15', 'pass15', 'test15@example.com'),
(16, 'test16', 'pass16', 'test16@example.com'),
(17, 'test17', 'pass17', 'test17@example.com'),
(18, 'test18', 'pass18', 'test18@example.com'),
(19, 'test19', 'pass19', 'test19@example.com'),
(20, 'test20', 'pass20', 'test20@example.com'),
(21, 'test21', 'pass21', 'test21@example.com'),
(22, 'admin', 'admin', 'admin@admin.admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
