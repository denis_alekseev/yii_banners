// alert(1);
function showForms(value) {
	switch(value) {
		case 'html':
			$('.value').show();
			$('.file').hide();
			$('.link').hide();
			break;
		case 'img':
			$('.value').hide();
			$('.file').show();
			$('.link').show();
			break;
		case 'swf':
			$('.value').hide();
			$('.file').show();
			$('.link').hide();
			break;		
	}
}
$(document).ready(function(){
	showForms($("#AdsBanners2_type").val());
	$("#AdsBanners2_type").change(function(){showForms($("#AdsBanners2_type").val())})
});