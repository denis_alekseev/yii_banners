<?php

/**
 * This is the model class for table "ads_banners2".
 *
 * The followings are the available columns in table 'ads_banners2':
 * @property integer $id
 * @property string $type
 * @property string $position
 * @property string $value
 * @property string $file
 * @property string $link
 * @property integer $counter
 * @property string $propertyType
 * @property string $dealKind
 * @property string $dealDirection
 * @property string $controller
 * @property string $action
 * @property integer $clicks
 */
class AdsBanners2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ads_banners2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, position', 'required'),
			array('counter, clicks', 'numerical', 'integerOnly'=>true),
			array('type, propertyType, dealKind, dealDirection, controller, action', 'length', 'max'=>20),
			array('value, file, link', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, position, value, file, link, counter, propertyType, dealKind, dealDirection, controller, action, clicks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'position' => 'Position',
			'value' => 'Value',
			'file' => 'File',
			'link' => 'Link',
			'counter' => 'Counter',
			'propertyType' => 'Property Type',
			'dealKind' => 'Deal Kind',
			'dealDirection' => 'Deal Direction',
			'controller' => 'Controller',
			'action' => 'Action',
			'clicks' => 'Clicks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('counter',$this->counter);
		$criteria->compare('propertyType',$this->propertyType,true);
		$criteria->compare('dealKind',$this->dealKind,true);
		$criteria->compare('dealDirection',$this->dealDirection,true);
		$criteria->compare('controller',$this->controller,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('clicks',$this->clicks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdsBanners2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	* Return random banner
	*
	* @param string $position
	*   Banner position name
	* @param string $propertyType
	*   Need comment
	* @param string $dealKind
	*   Need comment
	* @param string $dealDirection
	*   Need comment
	*
	* @return array
	*   random database record
	*/

	public function getRandomRow($position, $propertyType, $dealKind, $dealDirection) {
		$criteria = new CDbCriteria;
		$subcriteria = new CDbCriteria;
	
		$conroller = Yii::app()->controller->id;
		$action = Yii::app()->controller->action->id;
		$causes = array(
			'controller' => $conroller,
			'action' => $action,
		);
		$criteria->addSearchCondition('position', $position);
		
		if($conroller == 'property') {
			$causes += array(
				'propertyType' => $propertyType,
				'dealKind' => $dealKind,
				'dealDirection' => $dealDirection,
			);		
		}
		foreach($causes as $key=>$value) {
			$subcriteria->addInCondition($key,array($value, ''), 'AND');
		}
		$criteria->mergeWith($subcriteria);
		$criteria->order='RAND()';
		$criteria->limit=1;
		$res = $this->find($criteria);
		$res->counter++;
		$res->save();
		return $res->attributes;
	
	}

}
