<?php
class PropertyController extends Controller
{
/*	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}*/

	function actionShow($propertyType, $dealKind, $dealDirection) {
		$options = array(
			'propertyType' => $propertyType,
			'dealKind' => $dealKind,
			'dealDirection' => $dealDirection
		);
		$this->render('property', $options);
	 }

	function actionAway($id) {
		$linkRecord = new AdsBanners2;
		$link = $linkRecord->findByPk($id);
		$link->clicks++;
		$link->save();
		$this->redirect($link->link);
	}
}