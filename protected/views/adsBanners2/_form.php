<?php
/* @var $this AdsBanners2Controller */
/* @var $model AdsBanners2 */
/* @var $form CActiveForm */
Yii::app()->getClientScript()->registerCoreScript('jquery');  
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/assets/my.js',CClientScript::POS_END);

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ads-banners2-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php 
			$values = array(
				'html' => 'Html banner',
				'img' => 'image',
				'swf' => 'flash',
			);
			$attributes = array(
				'size' => 1,
			);
			echo $form->listBox($model,'type',$values,$attributes);
		?>
		<?php //echo $form->textField($model,'type',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php 
			$values = array(
				'top' => 'Top',
				'left' => 'Left',
				'bottom' => 'Bottom',
				'right' => 'Right'
			);
			$selected = explode(',',$model->position);
			$selectedList = array();
			foreach($selected as $item)
				$selectedList[$item] = array('selected' => 'selected');
			$attributes = array(
				'multiple' => 1,
				'options' => $selectedList,
			);
			echo $form->listBox($model,'position',$values,$attributes);
		?>
		<?php //echo $form->textField($model,'position',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>
	
	<div class="row value">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textArea($model,'value',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="row file">
		<?php echo $form->labelEx($model,'file'); ?>
		<?php echo $form->fileField($model,'file'); ?>
		<?php echo $form->error($model,'file'); ?>
	</div>

	<div class="row link">
		<?php echo $form->labelEx($model,'link'); ?>
		<?php echo $form->textArea($model,'link',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'link'); ?>
	</div>

	<div class="row">
		<?php echo $form->hiddenField($model,'counter',array('value'=>0)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'propertyType'); ?>
		<?php 
			$values = array(
				'' => '',
				'residential' => 'residential',
				'commercial' => 'commercial',
				'land' => 'land',
				'garage' => 'garage',
			);
			$attributes = array(
				'size' => 1,
			);
			echo $form->listBox($model,'propertyType',$values,$attributes);
		?>
		<?php //echo $form->textField($model,'position',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'propertyType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealKind'); ?>
		<?php 
			$values = array(
				'' => '',
				'sale' => 'sale',
				'rent' => 'rent',
			);
			$attributes = array(
				'size' => 1,
			);
			echo $form->listBox($model,'dealKind',$values,$attributes);
		?>
		<?php //echo $form->textField($model,'position',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'dealKind'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealDirection'); ?>
		<?php 
			$values = array(
				''=>'',
				'offer' => 'offer',
				'demand'=> 'demand',
			);
			$attributes = array(
				'size' => 1,
			);
			echo $form->listBox($model,'dealDirection',$values,$attributes);
		?>
		<?php //echo $form->textField($model,'position',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'dealDirection'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'controller'); ?>
		<?php echo $form->textField($model,'controller',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'controller'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action'); ?>
		<?php echo $form->textField($model,'action',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'action'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->