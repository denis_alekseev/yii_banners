<?php
/* @var $this AdsBanners2Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ads Banners2s',
);

$this->menu=array(
	array('label'=>'Create AdsBanners2', 'url'=>array('create')),
	array('label'=>'Manage AdsBanners2', 'url'=>array('admin')),
);
?>

<h1>Ads Banners2s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
