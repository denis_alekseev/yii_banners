<?php
/* @var $this AdsBanners2Controller */
/* @var $data AdsBanners2 */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('counter')); ?>:</b>
	<?php echo CHtml::encode($data->counter); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('propertyType')); ?>:</b>
	<?php echo CHtml::encode($data->propertyType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealKind')); ?>:</b>
	<?php echo CHtml::encode($data->dealKind); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealDirection')); ?>:</b>
	<?php echo CHtml::encode($data->dealDirection); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('controller')); ?>:</b>
	<?php echo CHtml::encode($data->controller); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action')); ?>:</b>
	<?php echo CHtml::encode($data->action); ?>
	<br />

	*/ ?>

</div>