<?php
/* @var $this AdsBanners2Controller */
/* @var $model AdsBanners2 */

$this->breadcrumbs=array(
	'Ads Banners2s'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdsBanners2', 'url'=>array('index')),
	array('label'=>'Create AdsBanners2', 'url'=>array('create')),
	array('label'=>'View AdsBanners2', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdsBanners2', 'url'=>array('admin')),
);
?>

<h1>Update AdsBanners2 <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>