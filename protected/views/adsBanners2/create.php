<?php
/* @var $this AdsBanners2Controller */
/* @var $model AdsBanners2 */

$this->breadcrumbs=array(
	'Ads Banners2s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdsBanners2', 'url'=>array('index')),
	array('label'=>'Manage AdsBanners2', 'url'=>array('admin')),
);
?>

<h1>Create AdsBanners2</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>