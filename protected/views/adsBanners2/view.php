<?php
/* @var $this AdsBanners2Controller */
/* @var $model AdsBanners2 */

$this->breadcrumbs=array(
	'Ads Banners2s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AdsBanners2', 'url'=>array('index')),
	array('label'=>'Create AdsBanners2', 'url'=>array('create')),
	array('label'=>'Update AdsBanners2', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdsBanners2', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdsBanners2', 'url'=>array('admin')),
);
?>

<h1>View AdsBanners2 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'position',
		'value',
		'file',
		'link',
		'counter',
		'propertyType',
		'dealKind',
		'dealDirection',
		'controller',
		'action',
	),
)); ?>
